﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CredentialServer.DTO;  //this is for controller which comes from DTO
using CredentialServer.DTO.Delete; //added to delete method
using CredentialServer.DTO.Insert; //added to insert method
using CredentialServer.DTO.ShowRow; //added to show row
using CredentialServer.DTO.Update; //added for update method
using CredentialServer.Processors; //it is for processor ActionDatabase class which linked to controller
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CredentialServer.Controllers
{
    [ApiController]// it comes by default telling this cs is api controller
    [Route("[controller]/[action]")] // this one also by default but we added action. route = www.fahad.com ---- /[controller] = Actions ---- /[action] = ShowAllRows. 
    //So, action is to show the rows to the users, while controller is the actions while route is the website link
    //our controller is something that communicate with the database 
    public class ActionsController : ControllerBase
    {
        IDatabaseActions actions = new DatabaseActions();  //it is contacting databaseaction.cs in processor and databaseactions is contacting the database workbench (all this in the backend in API).
        [HttpGet] //telling the api to select all rows from the table 
        public async Task<IActionResult> ShowAllRows() //it is related to Actionresults in UserInterface controllers. it is the barier so it can't see the code but it can get something from it (successful).  
        {
            Tuple<GeneralResponse, List<ShowRowResponse>> result = await actions.ShowAllRowsAsync("call credentialschema.ShowAllRows();"); //it goes to the schema in workbench. this is the benefit of stored procedure. actions is pointing at databaseaction. we can use (Async) and we do not have to at the same time, it does not change anything. ShowAllRowsAsync refer to the one in DatabaseActions
            if (result.Item1.ResultCode != 200) //The elements of a tuple can be accessed with using Item1, Item2 up to 7. So, here Item1 access the element 1
            {
                return StatusCode(result.Item1.ResultCode, result.Item1.ResultDescription); //if the result is not correct, then send us the error and descritopn.   
            }
            return Ok(result.Item2); // it is correct so send the list of rows to (the user interface) or might go to another API.  
        }
        [HttpPost] //send the row to the user interface. Only show one row (one credential data) in the web page. 
        public async Task<IActionResult> ShowRow(ShowRowRequest row) //IActionResult is something we return back. ShowRow() is what we are waiting for which is the object called row. 
        {
            Tuple<GeneralResponse, ShowRowResponse> result = await actions.ShowRowAsync($"call credentialschema.ShowSingleRow({row.CredentID});"); //actions is pointing at databaseaction.cs and it needs the object which is ID. await to wait. $ is like + but much better for readable and easy to write. When you want to hold a database record or some values temporarily without creating a separate class. ShowRowAsync refers to the one in DatabaseActions. 

            if (result.Item1.ResultCode != 200)  //The elements of a tuple can be accessed with using Item1, Item2 up to 7. So, here Item1 access the element 1
            {
                return StatusCode(result.Item1.ResultCode, result.Item1.ResultDescription); //if the result is not correct, then send us the error and descritopn.  
            }
            return Ok(result.Item2); // it is correct so send the list of rows to (the user interface) or might go to another API. 
        }

        [HttpPost]
        public async Task<IActionResult> Insert(InsertRequest insertRequest) //insert data into insert request (DTO)
        {                                                   // $ String interpolation is a better way of concatenating strings. We use + sign to concatenate string variables with static strings. C# 6 includes a special character $ to identify an interpolated string. An interpolated string is a mixture of static string and string variable where string variables should be in {} brackets. $ indicates the interpolated string, and {} includes string variable to be incorporated with a string.
            GeneralResponse result = await actions.InsertAsync($"call credentialschema.CreateNewRow('{insertRequest.UserName}', '{insertRequest.EncryptPW}', '{insertRequest.EKey}', '{insertRequest.WebsiteName}', '{insertRequest.WebsiteURL}', '{insertRequest.TStamp}', '{insertRequest.SelectedServer}');");
            if (result.ResultCode != 200) // we don't use Items because we didn't use Tuple 
            {
                return StatusCode(result.ResultCode, result.ResultDescription); 
            }
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> Update(UpdateRequest updateRequest) //update data into update request (DTO)
        {
            GeneralResponse result = await actions.UpdateAsync($"call credentialschema.UpdateRow('{updateRequest.CredentID}','{updateRequest.UserName}', '{updateRequest.EncryptPW}', '{updateRequest.EKey}', '{updateRequest.WebsiteName}', '{updateRequest.WebsiteURL}', '{updateRequest.TStamp}', '{updateRequest.SelectedServer}');");
            if (result.ResultCode != 200)
            {
                return StatusCode(result.ResultCode, result.ResultDescription);
            }
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(DeleteRequest updateRequest) //delete data (delete request (DTO)
        {
            GeneralResponse result = await actions.DeleteAsync($"call credentialschema.DeleteRow({updateRequest.CredentID});"); //delete the whole row
            if (result.ResultCode != 200)
            {
                return StatusCode(result.ResultCode, result.ResultDescription);
            }
            return Ok(result);
        }

    }

}

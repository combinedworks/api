﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; //this is for Required
using System.Linq;
using System.Threading.Tasks;

namespace CredentialServer.DTO.Delete
{
    public class DeleteRequest
    {
        [Required] //we have to add required to ensure that the user choose ID. 
        public int CredentID { get; set; } //the user needs to identfy a row using ID to delete a row so it is required.
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CredentialServer.DTO
{
    public class GeneralResponse
    {
        public int ResultCode { get; set; } //for integers response (200, 500)
        public string ResultDescription { get; set; } = string.Empty; //this is a replacement for response class in Delete, Insert and Update. we put it here for all of them. it is description of response
    }
}

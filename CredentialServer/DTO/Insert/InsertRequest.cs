﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;//this is for Required
using System.Linq;
using System.Threading.Tasks;

namespace CredentialServer.DTO.Insert
{
    public class InsertRequest
    {
        [Required] //each row here is required to be filled in by the user. stringEmpty because the field is not null.
        public string UserName { get; set; } = string.Empty;
        [Required] //each row here is required to be filled in by the user. stringEmpty because the field is not null.
        public string EncryptPW { get; set; } =string.Empty;

        public string EKey { get; set; } = string.Empty;
        [Required] //each row here is required to be filled in by the user. stringEmpty because the field is not null.
        public string WebsiteName { get; set; } = string.Empty;
        [Required] //each row here is required to be filled in by the user. stringEmpty because the field is not null.
        public string WebsiteURL { get; set; } = string.Empty;

        public string TStamp { get; set; } = string.Empty; //it is from Json TStamp. C# likes to have the first letter as uppercase
        public string SelectedServer { get; set; } = string.Empty; //this is for selected server
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CredentialServer.DTO.ShowRow
{
    public class ShowRowResponse
    {
        //it will show all rows to the user as a response
        public int CredentID { get; set; } 
        public string UserName { get; set; } = string.Empty;
        public string EncryptPW { get; set; } = string.Empty;
        public string EKey { get; set; } = string.Empty;
        public string WebsiteName { get; set; } = string.Empty;
        public string WebsiteURL { get; set; } = string.Empty;
        public string TStamp { get; set; } = string.Empty;
        public string SelectedServer { get; set; } = string.Empty; 
    }
}

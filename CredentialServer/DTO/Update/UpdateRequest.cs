﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;//this is for Required
using System.Linq;
using System.Threading.Tasks;

namespace CredentialServer.DTO.Update
{
    public class UpdateRequest
    {
        [Required] //we have to add required to ensure that the user choose ID. 
        public int CredentID { get; set; } //the user needs to identfy a row using ID to delete a row so it is required.
        [Required] //we have to add required to ensure that the user choose ID. 
        public string UserName { get; set; } = string.Empty;
        [Required] //each row here is required to be filled in by the user. stringEmpty because the field is not null.
        public string EncryptPW { get; set; } = string.Empty;

        public string EKey { get; set; } = string.Empty; //remove required 
        [Required] //we have to add required to ensure that the user choose ID. 
        public string WebsiteName { get; set; } = string.Empty;
        [Required] //we have to add required to ensure that the user choose ID. 
        public string WebsiteURL { get; set; } = string.Empty;

        public string TStamp { get; set; } = string.Empty;
        public string SelectedServer { get; set; } = string.Empty; //this is for selected server
    }
}

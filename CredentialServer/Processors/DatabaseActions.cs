﻿using CredentialServer.DTO;  //this one if for DTO in general
using CredentialServer.DTO.ShowRow; // this for GeneralResponse shpw row
using System;
using System.Collections.Generic; //for interface and increases the reusability of the code.
using System.Data;
using System.Data.Odbc; //it is for Odbc connection between Workbench and API
//using System.Data.OdbcClient;  
using System.Linq;
using System.Threading.Tasks;

namespace CredentialServer.Processors  // it is refering to connection class
{                                   //IDatabaseActions is under processors in here. It declares functionality because an interface can only contain declarations but not implementations. Do not include 'public' in an interface as all the members are public by default.
    interface IDatabaseActions   //this is the interface of class. In C#, an interface can be defined using the interface keyword. Interfaces can contain methods, properties, indexers, and events as members.the interface includes the declaration of one or more functionalities.
    {
        //Tuple is used when you want to hold a database record or some values temporarily without creating a separate class.
        //A tuple is a data structure that contains a sequence of elements of different data types. It can be used where you want to have a data structure to hold an object with properties, but you don't want to create a separate type for it
        Task<Tuple<GeneralResponse, List<ShowRowResponse>>> ShowAllRowsAsync(string OdbcQuery); // Receivng and delivering request. we can use (Async) and we do not have to at the same time, it does not change anything. GeneralResponse for 200,500 and ShowRowResponse is to show data
        Task<Tuple<GeneralResponse, ShowRowResponse>> ShowRowAsync(string OdbcQuery); //we do not use List because we do not need the list. ShowRowAsync is linked to Controller. 
        Task<GeneralResponse> InsertAsync(string OdbcQuery); //this is the interface for insert
        Task<GeneralResponse> UpdateAsync(string OdbcQuery); // this is for the update interface
        Task<GeneralResponse> DeleteAsync(string OdbcQuery); // this is for delete interface
    }

    public class DatabaseActions : IDatabaseActions  // first one is child, second one is parents. it does all the work here between server and database workbench. This class is from the father's class Interface IDatabaseActions
    {
        // using async to improve performance and scalability
        public async Task<Tuple<GeneralResponse, List<ShowRowResponse>>> ShowAllRowsAsync(string OdbcQuery) //we use Async method to avoid any problem with loading and our screen doesn't stuck and free the thread otherwise the program will crash. Odbc query is a name of variable we give it.
        {
            try
            {
                //telling the connection where the string is, we put what we have in DNS class in here. 
                List<ShowRowResponse> rowResponses = new List<ShowRowResponse>();
                string connectionString = Connection.connectionString;  // we can cut down lines but I want to keep it simple. "connectionString" is refering to connection class where it is made to database (connection class)
                OdbcConnection makeOdbcConn = new OdbcConnection(connectionString); //makeOdbcConn is for connection, connectionString is a variable name refering to Connection Class. 
                using (makeOdbcConn)//using is used for as soon as something happens betweeen database and server, it will kill the thread so there is no further error. (garbage collector to clean the miss). makeOdbcConn is a variable name (object)
                {
                    makeOdbcConn.Open(); //makeOdbcConn is a variable name and Opens is a method which opens a FileStream on the specified path with read/write access.
                    OdbcCommand cmd = new OdbcCommand(OdbcQuery, makeOdbcConn); //cmd is a name we give it to the command
                    using OdbcDataReader reader = (OdbcDataReader)await cmd.ExecuteReaderAsync(); //I clicked on 3 green dots to simplify it. ExecuteReaderAsync sends the CommandText to the Connection and builds a SqlDataReader. Exceptions will be reported via the returned Task object.
                    while (reader.Read())//while that bag has apple, it will look through them but it might be no apple which is fine. reader is a variable name (Hand Shake which is mapping, like mapping A in Arabic to A in English)
                    {
                        //we got the information from database, while we have some rows provided by database then let's go thourgh every row, let's grab the first row which is the ID (0). next line is (1) which is the username. 
                        //we have to keep this codes to make it useful as they are requested by users to give them this information regarding an apple.
                        ShowRowResponse singleRow = new ShowRowResponse //I clicked on 3 green lines and asked me to choose simiplfy codes which makes it looks better.
                        {
                            //GetValue returns an array of the values of all the constants of specified enum.
                            CredentID = Convert.ToInt32(reader.GetValue(0).ToString()), //convert it to string. CredentID is from SQL database
                            UserName = reader.GetValue(1).ToString(), //convert it to string. UserName is from SQL database
                            EncryptPW = reader.GetValue(2).ToString(), //convert it to string. EncryptPW is from SQL database
                            EKey = reader.GetValue(3).ToString(), //convert it to string. EKey is from SQL database
                            WebsiteName = reader.GetValue(4).ToString(), //convert it to string. WebsiteName is from SQL database
                            WebsiteURL = reader.GetValue(5).ToString(), //convert it to string. WebsiteURL is from SQL database
                            TStamp = reader.GetValue(6).ToString(), //convert it to string. TStamp is from SQL database
                            SelectedServer = reader.GetValue(7).ToString() //convert it to string. SelectedServer is from SQL database
                        };
                        rowResponses.Add(singleRow); //you add a new row inside our list. 
                    }

                }
                if (rowResponses.Count == 0) //if the list is empty then we have no rows. we asked database to provide us with something so we make sure there is nothing or error. If I don't use if condition, then it will be an error for waiting 
                {
                    GeneralResponse gR = new GeneralResponse   //in Server side, I used (Exception) 200 and 500 so I can test the connection with Database using Postman.  
                    {
                        ResultCode = 200, //it means it is fine
                        ResultDescription = "NoRows" // we have no rows
                    };
                    return Tuple.Create(gR, rowResponses);
                }
                else  //we get rows here and sending back the list but the message 
                {
                    GeneralResponse gR = new GeneralResponse
                    {
                        ResultCode = 200, // it is fine
                        ResultDescription = "Success" // it is fine and we have rows
                    };
                    return Tuple.Create(gR, rowResponses);
                }
            }
            catch (Exception error) 
            {
                GeneralResponse gR = new GeneralResponse
                {
                    ResultCode = 500,
                    ResultDescription = error.ToString()
                };
                return Tuple.Create(gR, new List<ShowRowResponse>());
            }
        }
        public async Task<Tuple<GeneralResponse, ShowRowResponse>> ShowRowAsync(string OdbcQuery) //we removed list because we do not need it in show row
        {
            try
            {
                ShowRowResponse row = new ShowRowResponse();
                string connectionString = Connection.connectionString;
                OdbcConnection makeOdbcConn = new OdbcConnection(connectionString);
                using (makeOdbcConn)
                {
                    makeOdbcConn.Open();
                    OdbcCommand cmd = new OdbcCommand(OdbcQuery, makeOdbcConn);
                    using (OdbcDataReader reader = (OdbcDataReader)await cmd.ExecuteReaderAsync())
                    {
                        while (reader.Read())
                        {
                            row = new ShowRowResponse
                            {
                                CredentID = Convert.ToInt32(reader.GetValue(0).ToString()), //convert it to string. CredentID is from SQL database
                                UserName = reader.GetValue(1).ToString(), //convert it to string. UserName is from SQL database
                                EncryptPW = reader.GetValue(2).ToString(), //convert it to string. EncryptPW is from SQL database
                                EKey = reader.GetValue(3).ToString(), //convert it to string. EKey is from SQL database
                                WebsiteName = reader.GetValue(4).ToString(), //convert it to string. WebsiteName is from SQL database
                                WebsiteURL = reader.GetValue(5).ToString(), //convert it to string. WebsiteURL is from SQL database
                                TStamp = reader.GetValue(6).ToString(), //convert it to string. TStamp is from SQL database
                                SelectedServer = reader.GetValue(7).ToString() //convert it to string. SelectedServer is from SQL database
                            };
                        }

                    }
                }
                GeneralResponse gR = new GeneralResponse
                {
                    ResultCode = 200,
                    ResultDescription = "Success"
                };
                return Tuple.Create(gR, row);

            }
            catch (Exception error)
            {
                GeneralResponse gR = new GeneralResponse
                {
                    ResultCode = 500,
                    ResultDescription = error.ToString()
                };
                return Tuple.Create(gR, new ShowRowResponse());
            }
        }
        public async Task<GeneralResponse> InsertAsync(string OdbcQuery) //we don't use tuple because there is no list, also no show response
        {
            return await DbIncUpdDel(OdbcQuery); //db refers to database, Inc for insert, Upd is for update, Del is for Delete. we don't have to add more codes. it goes to private class below
        }
        public async Task<GeneralResponse> UpdateAsync(string OdbcQuery) 
        {
            return await DbIncUpdDel(OdbcQuery); //db refers to database, Inc for insert, Upd is for update, Del is for Delete. we don't have to add more codes. it goes to private class below. 
        }
        public async Task<GeneralResponse> DeleteAsync(string OdbcQuery) 
        {
            return await DbIncUpdDel(OdbcQuery); //db refers to database, Inc for insert, Upd is for update, Del is for Delete. we don't have to add more codes. it goes to private class below
        }
        private async Task<GeneralResponse> DbIncUpdDel(string OdbcQuery) //private class for Insert, Update and Delete. 
        {
            try
            {
                bool done = false; // we did not run the query for some reason. so everytghing remains the same. done is a variable (false means nothing happen).
                string connectionString = Connection.connectionString;
                OdbcConnection makeOdbcConn = new OdbcConnection(connectionString);
                using (makeOdbcConn)
                {
                    makeOdbcConn.Open();
                    OdbcCommand cmd = new OdbcCommand(OdbcQuery, makeOdbcConn);
                    using (OdbcDataReader reader = (OdbcDataReader)await cmd.ExecuteReaderAsync())
                    {
                        done = true; //now, there is something happens here, so we go to the next statement. 

                    }
                }
                if (done == true) //if something is inserted, update or deleted, then it is true and the code will be excecuted. 
                {
                    return new GeneralResponse //it is a class that for insert, update and delete which is successful
                    {
                        ResultCode = 200, //correct
                        ResultDescription = "Success" //description
                    };
                }
                return new GeneralResponse //if not, perhaps there is an error
                {
                    ResultCode = 500, //we send 500 back 
                    ResultDescription = "Error" //but we don't know what error but don't go to the catch. it helps us trying to find the probelm which refers to "Error".  
                };
            }
            catch (Exception error) // if go to the catch, we will know what the error is. for example, it will tell us that you are trying to force to  int to string. 
            {
                return new GeneralResponse
                {
                    ResultCode = 500, //treated as error
                    ResultDescription = error.ToString()
                };
            }
        }

        //The above set of codes reduce the amount of line in this set, so I removed the below codes. In above codes, we use private class to execute order from public class. 
        /*public async Task<GeneralResponse> InsertAsync(string OdbcQuery)
        {
            try
            {
                bool done = false;
                string connectionString = Connection.connectionString;
                OdbcConnection makeOdbcConn = new OdbcConnection(connectionString);
                var rowSingle = new GeneralResponse();
                using (makeOdbcConn)
                {
                    makeOdbcConn.Open();
                    OdbcCommand cmd = new OdbcCommand(OdbcQuery, makeOdbcConn);
                    using (OdbcDataReader reader = (OdbcDataReader)await cmd.ExecuteReaderAsync())
                    {
                        done = true;
                    }
                }
                if (done == true)
                {
                    return new GeneralResponse
                    {
                        ResultCode = 200, //correct
                        ResultDescription = "Success" //description
                    };
                }
                return new GeneralResponse
                {
                    ResultCode = 500, //we send 500 back 
                    ResultDescription = "Error"
                };
            }
            catch (Exception error)
            {
                return new GeneralResponse
                {
                    ResultCode = 500, //treated as error
                    ResultDescription = error.ToString()
                };
            }
        }
        public async Task<GeneralResponse> UpdateAsync(string OdbcQuery)
        {
            try
            {
                bool done = false;
                string connectionString = Connection.connectionString;
                OdbcConnection makeOdbcConn = new OdbcConnection(connectionString);
                var rowSingle = new GeneralResponse();
                using (makeOdbcConn)
                {
                    makeOdbcConn.Open();
                    OdbcCommand cmd = new OdbcCommand(OdbcQuery, makeOdbcConn);
                    using (OdbcDataReader reader = (OdbcDataReader)await cmd.ExecuteReaderAsync())
                    {
                        done = true;
                    }
                }
                if (done == true)
                {
                    return new GeneralResponse
                    {
                        ResultCode = 200, //correct
                        ResultDescription = "Success" //description
                    };
                }
                return new GeneralResponse
                {
                    ResultCode = 500, //we send 500 back 
                    ResultDescription = "Error"
                };
            }
            catch (Exception error)
            {
                return new GeneralResponse
                {
                    ResultCode = 500, //treated as error
                    ResultDescription = error.ToString()
                };
            }
        }
        public async Task<GeneralResponse> DeleteAsync(string OdbcQuery)
        {
            try
            {
                bool done = false;
                string connectionString = Connection.connectionString;
                OdbcConnection makeOdbcConn = new OdbcConnection(connectionString);
                var rowSingle = new GeneralResponse();
                using (makeOdbcConn)
                {
                    makeOdbcConn.Open();
                    OdbcCommand cmd = new OdbcCommand(OdbcQuery, makeOdbcConn);
                    using (OdbcDataReader reader = (OdbcDataReader)await cmd.ExecuteReaderAsync())
                    {
                        done = true;
                    }
                }
                if (done == true)
                {
                    return new GeneralResponse
                    {
                        ResultCode = 200, //correct
                        ResultDescription = "Success" //description
                    };
                }
                return new GeneralResponse
                {
                    ResultCode = 500, //we send 500 back 
                    ResultDescription = "Error"
                };
            }
            catch (Exception error)
            {
                return new GeneralResponse
                {
                    ResultCode = 500, //treated as error
                    ResultDescription = error.ToString()
                };
            }

        }*/
    }


}
